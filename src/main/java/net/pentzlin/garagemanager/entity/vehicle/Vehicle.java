package net.pentzlin.garagemanager.entity.vehicle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.pentzlin.garagemanager.entity.garage.ParkingPlace;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "vehicle_type")
public abstract class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "license_plate", nullable = false, unique = true)
    private String licensePlate;

    @Column(name = "vehicle_type", insertable = false, updatable = false)
    private String vehicleType;

    @OneToOne(mappedBy = "parkedVehicle", cascade = CascadeType.ALL)
    private ParkingPlace parkingPlace;

    public Vehicle() {}
    public Vehicle(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getId() {
        return id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @JsonIgnore
    public ParkingPlace getParkingPlace() {
        return parkingPlace;
    }

    public void setParkingPlace(ParkingPlace parkingPlace) {
        this.parkingPlace = parkingPlace;
    }

    public String getVehicleType() {
        return vehicleType;
    }
}
